package com.hxx.stock.mapper;

import com.hxx.stock.pojo.domain.InnerMarketDomain;
import com.hxx.stock.pojo.domain.StockBlockDomain;
import com.hxx.stock.pojo.entity.StockMarketIndexInfo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author songx
* @description 针对表【stock_market_index_info(国内大盘数据详情表)】的数据库操作Mapper
* @createDate 2024-05-13 16:01:30
* @Entity com.hxx.stock.pojo.entity.StockMarketIndexInfo
*/
public interface StockMarketIndexInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockMarketIndexInfo record);

    int insertSelective(StockMarketIndexInfo record);

    StockMarketIndexInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockMarketIndexInfo record);

    int updateByPrimaryKey(StockMarketIndexInfo record);

/**

 * @author songx
 * @description TODO：跟据指定的时间点查询指定大盘编码对应的数据
 * @date 2024/5/19 14:17
 * @param curDate:指定的时间
 * @param marketCodes:大盘编码集合
 */
    List<InnerMarketDomain> getMarketInfo(@Param("curDate")Date curDate, @Param("marketCodes")List<String> marketCodes);
    /**
     * @author songx
     * @description TODO：统计大盘每分钟的成交量
     * @date 2024/5/19 14:17
     * @param openDate:起始时间
     * @param endDate:截止时间
     */
    List<Map> getSumAmtInfo(@Param("openDate") Date openDate, @Param("endDate") Date endDate, @Param("marketCodes") List<String> marketCodes);
    /**
     * @author songx
     * @description TODO：批量插入大盘数据
     * @date 2024/5/19 14:17
     * @param entities 大盘实体集合
     */
    int insertBatch(@Param("entities") List<StockMarketIndexInfo> entities);
}

