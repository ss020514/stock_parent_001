package com.hxx.stock.mapper;
import com.hxx.stock.pojo.domain.Stock4EvrDayDomain;
import com.hxx.stock.pojo.domain.Stock4MinuteDomain;
import com.hxx.stock.pojo.domain.StockBlockDomain;
import com.hxx.stock.pojo.domain.StockUpdownDomain;
import com.hxx.stock.pojo.entity.StockRtInfo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author songx
* @description 针对表【stock_rt_info(个股详情信息表)】的数据库操作Mapper
* @createDate 2024-05-13 16:01:30
* @Entity com.hxx.stock.pojo.entity.StockRtInfo
*/
public interface StockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockRtInfo record);

    int insertSelective(StockRtInfo record);

    StockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockRtInfo record);

    int updateByPrimaryKey(StockRtInfo record);
    /*
     * @author songx
     * @description TODO：查询指定时间点下股票数据集合
     * @param curDate日期时间
     * @date 2024/5/20 17:23
     */

    List<StockUpdownDomain> getStockInfoByTime(@Param("curDate") Date curDate);
/*

 * @author songx
 * @description 统计指定日期范围内股票的涨停与跌停的数量流水
 * @param starDate 开始时间 即指开盘时间
 * @param endDate  截止时间
 * @param flag 约定 1代表涨停，0代表跌停
 * @date 2024/5/20 22:05
 */
    List<Map> getStockUpdownCount(@Param("starDate") Date starDate, @Param("endDate") Date endDate, @Param("flag") int flag);

    List<StockUpdownDomain> stockIncreaseBy4(@Param("curDate") Date curDate);
 /**
  * @author songx
  * @description 统计指定时间点下股票在各个涨跌区间的数据
  * @param curDate 当前时间
  * @date 2024/5/25 15:47
  */
    List<Map> getIncreaseRangeInfoByDate(@Param("curDate") Date curDate);
    /*
     * @author songx
     * @description 跟据股票编码查询指定时间范围内的分时数据
     * @param stockCode 个股股票编码
     * @param startDate 开盘时间
     * @param endDate 最新时间
     * @date 2024/5/22 19:58
     */
    List<Stock4MinuteDomain> getStock4MinuteInfo(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("stockCode") String stockCode);
    /*
     * @author songx
     * @description 跟据编码查询指定时间范围的日K线数据
     * @param stockCode 个股股票编码
     * @param startDate 开始时间
     * @param endDate 最新时间
     * @date 2024/5/22 19:58
     */
    List<Stock4EvrDayDomain> getStock4DKLine(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("stockCode") String stockCode);
    /*
     * @author songx
     * @description 获取日期范围内的收盘日期
     * @param stockCode 个股股票编码
     * @param startDate 开始时间
     * @param endDate 最新时间
     * @date 2024/5/22 19:58
     */
    List<Date> getCloseDates(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("code") String stockCode);
    /**
     * 获取指定股票在指定日期点下的数据
     * @param code 股票编码
     * @param dates 指定日期集合
     * @return
     */
    List<Stock4EvrDayDomain> getStockCreenDkLineData(@Param("code") String code,
                                                     @Param("dates") List<Date> dates);
    /**
     * 批量插入个股数据
     * @param list
     * @return
     */
    int insertBatch(@Param("list") List<StockRtInfo> list);
}

