package com.hxx.stock.mapper;

import com.hxx.stock.pojo.domain.StockBlockDomain;
import com.hxx.stock.pojo.entity.StockBlockRtInfo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
* @author songx
* @description 针对表【stock_block_rt_info(股票板块详情信息表)】的数据库操作Mapper
* @createDate 2024-05-13 16:01:30
* @Entity com.hxx.stock.pojo.entity.StockBlockRtInfo
*/
public interface StockBlockRtInfoMapper {


     List<StockBlockDomain> sectorAll(@Param("curDate")Date curDate);

    int deleteByPrimaryKey(Long id);

    int insert(StockBlockRtInfo record);

    int insertSelective(StockBlockRtInfo record);

    StockBlockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBlockRtInfo record);

    int updateByPrimaryKey(StockBlockRtInfo record);
    /**
     * 板块信息批量插入
     * @param list 板块数据集合
     * @return
     */
    int insertBatch(@Param("list") List<StockBlockRtInfo> list);


}
