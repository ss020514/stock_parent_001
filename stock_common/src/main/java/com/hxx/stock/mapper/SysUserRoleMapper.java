package com.hxx.stock.mapper;

import com.hxx.stock.pojo.entity.SysUserRole;

/**
* @author songx
* @description 针对表【sys_user_role(用户角色表)】的数据库操作Mapper
* @createDate 2024-05-13 16:01:30
* @Entity com.hxx.stock.pojo.entity.SysUserRole
*/
public interface SysUserRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);

}
