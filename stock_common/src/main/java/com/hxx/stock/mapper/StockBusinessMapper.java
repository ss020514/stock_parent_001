package com.hxx.stock.mapper;

import com.hxx.stock.pojo.entity.StockBusiness;

import java.util.List;

/**
* @author songx
* @description 针对表【stock_business(主营业务表)】的数据库操作Mapper
* @createDate 2024-05-13 16:01:30
* @Entity com.hxx.stock.pojo.entity.StockBusiness
*/
public interface StockBusinessMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockBusiness record);

    int insertSelective(StockBusiness record);

    StockBusiness selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBusiness record);
    int updateByPrimaryKey(StockBusiness record);
    /*
     * @author songx
     * @description :获取所有的A股个股编码
     * @date 2024/5/29 15:32
     */
    List<String> getAllStockCodes();

}
