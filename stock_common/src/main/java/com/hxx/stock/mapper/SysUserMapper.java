package com.hxx.stock.mapper;

import com.hxx.stock.pojo.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author songx
* @description 针对表【sys_user(用户表)】的数据库操作Mapper
* @createDate 2024-05-13 16:01:30
* @Entity com.hxx.stock.pojo.entity.SysUser
*/
public interface SysUserMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    SysUser findUserInfoByUserName(@Param("userName") String userName);

    SysUser findByUserName(@Param("username") String username);
    //查询所有用户信息
    List<SysUser> findAll();
}
