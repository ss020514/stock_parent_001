package com.hxx.stock.pojo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.annotations.Insert;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author song
 * @Date 2024/5/19 20:17
 * @Description:股票板块Domain
 */
@ApiModel(description = "股票板块Domain")
@Data
public class StockBlockDomain {

    @ApiModelProperty(value = "公司数量")
    private Integer companyNum;//公司数量
    @ApiModelProperty(value = "交易量")
    private Long tradeAmt;//交易量
    @ApiModelProperty(value = "股票编码")
    private String code;//股票编码
    @ApiModelProperty(value = "平均价")
    private BigDecimal avgPrice;//平均价
    @ApiModelProperty(value = "板块名字")
    private String name;//板块名字
    @ApiModelProperty(value = "当前日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date curDate;//当前日期
    @ApiModelProperty(value = "交易金额")
    private BigDecimal tradeVol;//交易金额
    @ApiModelProperty(value = "涨跌率")
    private BigDecimal updownRate;//涨跌率
}
