package com.hxx.stock.pojo.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author song
 * @Date 2024/5/20 15:06
 * @Description:股票涨跌信息
 */

@ApiModel(description = "股票涨跌信息")
@Data
public class StockUpdownDomain {
    @ExcelProperty(value = {"股票涨幅信息统计表","股票编码"},index = 0)
    @ApiModelProperty(value = "股票编码")
    private String code;//股票编码
    @ExcelProperty(value = {"股票涨幅信息统计表","股票名称"},index = 1)
    @ApiModelProperty(value = "股票名称")
    private String name;//股票名称
    @ExcelProperty(value = {"股票涨幅信息统计表","前收盘价"},index = 2)
    @ApiModelProperty(value = "前收盘价")
    private BigDecimal preClosePrice;//前收盘价
    @ExcelProperty(value = {"股票涨幅信息统计表","当前价格"},index = 3)
    @ApiModelProperty(value = "当前价格")
    private BigDecimal tradePrice;//当前价格
    @ExcelProperty(value = {"股票涨幅信息统计表","涨跌"},index = 4)
    @ApiModelProperty(value = "涨跌")
    private BigDecimal increase;//涨跌
    @ExcelProperty(value = {"股票涨幅信息统计表","涨幅"},index = 5)
    @ApiModelProperty(value = "涨幅")
    private BigDecimal upDown;//涨幅
    @ExcelProperty(value = {"股票涨幅信息统计表","振幅"},index = 6)
    @ApiModelProperty(value = "振幅")
    private BigDecimal amplitude;//振幅
    @ExcelProperty(value = {"股票涨幅信息统计表","交易量"},index = 7)
    @ApiModelProperty(value = "交易量")
    private Long tradeAmt;//交易量
    @ExcelProperty(value = {"股票涨幅信息统计表","交易金额"},index = 8)
    @ApiModelProperty(value = "交易金额")
    private BigDecimal tradeVol;//交易金额

    /**
     * 日期
     */
    @ExcelProperty(value = {"股票涨幅信息统计表","日期"})
    @DateTimeFormat("yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date curDate;
}
