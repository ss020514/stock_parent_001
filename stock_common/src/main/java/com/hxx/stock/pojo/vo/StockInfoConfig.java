package com.hxx.stock.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author song
 * @Date 2024/5/19 12:45
 * @Description:定义股票相关的值对象封装
 */
@ApiModel(description = "定义股票相关的值对象封装")
@Data
@ConfigurationProperties(prefix ="stock")
public class StockInfoConfig {
    @ApiModelProperty(value = "封装国内大盘编码集合", position = 1)
    private List<String>inner; //封装国内大盘编码集合
    @ApiModelProperty(value = "外盘编码集合", position = 2)
    private  List<String>outer;//外盘编码集合
    @ApiModelProperty(value = "股票涨幅区间标题" ,hidden = true)
    private List<String> upDownRange;
    /**
     * @description 大盘，外盘，个股的公共Url
     * @date 2024/5/26 19:26
     */
    @ApiModelProperty(value = "大盘，外盘，个股的公共Url", position = 4)
    private  String marketUrl;
    /**
     * @description 板块采集URL
     * @date 2024/5/26 19:26
     */
    @ApiModelProperty(value = "板块采集URL", position = 5)
    private String blockUrl;

}
