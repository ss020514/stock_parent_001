package com.hxx.stock;

import com.google.common.collect.Lists;
import com.hxx.stock.mapper.StockBusinessMapper;
import com.hxx.stock.service.StockTimerTaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author song
 * @Date 2024/5/29 19:47
 * @Description:
 */
@SpringBootTest
public class TestMapper {
    @Autowired
    private StockBusinessMapper stockBusinessMapper;
    @Autowired
    StockTimerTaskService stockTimerTaskService;
    /**
     * @author songx
     * @description 测试股票个股的编码集合
     * @date 2024/5/29 19:49
     */
    @Test
    public void text01(){
        List<String> allCodes = stockBusinessMapper.getAllStockCodes();
        System.out.println(allCodes);
        //添加大盘业务前缀 sh,sz
        allCodes=allCodes.stream().map(code->code.startsWith("6")?"sh"+code:"sz"+code).collect(Collectors.toList());
        System.out.println(allCodes);
        //将所有个股编码组成大的集合拆分成若干小的集合
        Lists.partition(allCodes,15).forEach(codes->{
            System.out.println("size:"+codes.size()+codes);
        });

    }
    @Test
    public void text02(){
        stockTimerTaskService.getStockBlockInfo();
    }

}
