package com.hxx.stock;

import com.hxx.stock.config.CommonConfig;
import com.hxx.stock.mapper.StockMarketIndexInfoMapper;
import com.hxx.stock.pojo.Account;
import com.hxx.stock.pojo.vo.StockInfoConfig;
import com.hxx.stock.service.StockTimerTaskService;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @Author song
 * @Date 2024/5/26 16:41
 * @Description:
 */
@SpringBootTest
public class TestRestTemplate {
   @Autowired
   private RestTemplate restTemplate;
   @Autowired
   private StockTimerTaskService stockTimerTaskService;
    @Autowired
    private CommonConfig commonConfig;
    @Autowired
    private StockInfoConfig stockInfoConfig;

/*

 * @author songx
 * @description 测试get
 * @date 2024/5/26 16:45
 */
    @Test
    public void test01(){
        String url="http://localhost:6767/account/getByUserNameAndAddress?userName=zhangsan&address=sh";
        ResponseEntity<String> resp = restTemplate.getForEntity(url, String.class);
        int statusCodeValue = resp.getStatusCodeValue();
        System.out.println(statusCodeValue);
        String body = resp.getBody();
        System.out.println(body);

    }
    @Test
    public void test02(){
        String url="http://localhost:6767/account/getByUserNameAndAddress?userName=zhangsan&address=sh";
        //直接获取响应数据
        Account account = restTemplate.getForObject(url, Account.class);
        System.out.println(account);

    }
    /*
     * @author songx
     * @description TODO: 测试采集大盘数据
     * @date 2024/5/27 16:07
     */
    @Test
    public void testInnerGetMarketInfo(){
        //stockTimerTaskService.getInnerMarketInfo();
        stockTimerTaskService.getStockRtInfo();
    }
}
