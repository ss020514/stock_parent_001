package com.hxx.stock.config;

import com.hxx.stock.pojo.vo.StockInfoConfig;
import com.hxx.stock.util.IdWorker;
import com.hxx.stock.util.ParserStockInfoUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @Author song
 * @Date 2024/5/14 13:50
 * @Description:定义公共配置bean
 */
@Configuration
@EnableConfigurationProperties({StockInfoConfig.class})//开启对象相关配置类的开启
public class CommonConfig {
    /*
     * @author songx
     * @description TODO：定义密码加密匹配器
     * @date 2024/5/14 13:53
     */
//    @Bean
//    public PasswordEncoder passwordEncoder(){
//        return new BCryptPasswordEncoder();
//    }
    /*
     * @author songx
     * @description TODO：基于雪花算法的工具类保证生成的id唯一
     * @date 2024/5/15 15:18
     */
    @Bean
    public IdWorker idWorker(){
        //基于运维人员对机房和机器的编号规划自行约定
        return new IdWorker(1l,2l);
    }
    /*
     * @author songx
     * @description 定义解析股票大盘，外盘个股，板块相关工具类bean
     * @date 2024/5/29 20:18
     */
    @Bean
    public ParserStockInfoUtil parserStockInfoUtil(IdWorker idWorker){
        return  new ParserStockInfoUtil(idWorker);
    }
}
