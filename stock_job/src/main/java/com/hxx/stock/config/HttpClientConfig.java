package com.hxx.stock.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Author song
 * @Date 2024/5/26 16:08
 * @Description:定义http客户端工具bean
 */
@Configuration
public class HttpClientConfig {
    /*
     * @author songx
     * @description 定义HTTP客户端bean
     * @date 2024/5/26 16:09
     */
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
