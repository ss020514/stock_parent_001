package com.hxx.stock.job;

import com.hxx.stock.service.StockTimerTaskService;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author song
 * @Date 2024/6/1 19:37
 * @Description:定义xxljob任务执行器bean
 */
@Component
public class StockJob {
    @Autowired
    StockTimerTaskService stockTimerTaskService;

    @XxlJob("myInnerJobHandler")
    public void demoJobHandler() throws Exception {
        System.out.println("当前时间："+ DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }
    /*
     * @author songx
     * @description 定时采集国内A股大盘数据
     * @date 2024/6/1 22:58
     */
    @XxlJob("getInnerStockInfo")
    public void getStockInfo(){
        stockTimerTaskService.getInnerMarketInfo();

    }
    /*
     * @author songx
     * @description 定时采集个股数据
     * @date 2024/6/1 22:58
     */
    @XxlJob("getStockRtInfo")
    public void getStockRtInfo(){
        stockTimerTaskService.getStockRtInfo();
    }
    /*
     * @author songx
     * @description 定时采集板块数据
     * @date 2024/6/1 22:58
     */
    @XxlJob("getStockBlockInfo")
    public void getStockBlockInfo(){
        stockTimerTaskService.getStockBlockInfo();
    }
}
