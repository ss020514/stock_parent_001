package com.hxx.stock.service.impl;

import com.google.common.collect.Lists;
import com.hxx.stock.constant.ParseType;
import com.hxx.stock.mapper.StockBlockRtInfoMapper;
import com.hxx.stock.mapper.StockBusinessMapper;
import com.hxx.stock.mapper.StockMarketIndexInfoMapper;
import com.hxx.stock.mapper.StockRtInfoMapper;
import com.hxx.stock.pojo.entity.StockBlockRtInfo;
import com.hxx.stock.pojo.entity.StockMarketIndexInfo;
import com.hxx.stock.pojo.entity.StockRtInfo;
import com.hxx.stock.pojo.vo.StockInfoConfig;
import com.hxx.stock.service.StockTimerTaskService;
import com.hxx.stock.util.DateTimeUtil;
import com.hxx.stock.util.IdWorker;
import com.hxx.stock.util.ParserStockInfoUtil;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @Author
 * @Date 2024/5/27 15:46
 * @Description:获取国内大盘的实时数据信息
 */
@Service
@Slf4j
public class StockTimerTaskServiceImpl implements StockTimerTaskService {
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    StockInfoConfig stockInfoConfig;
    @Autowired
    IdWorker idWorker;
    @Autowired
   private StockMarketIndexInfoMapper stockMarketIndexInfoMapper;
    @Autowired
    private StockBusinessMapper stockBusinessMapper;
    @Autowired
    ParserStockInfoUtil parserStockInfoUtil;
    @Autowired
    StockRtInfoMapper stockRtInfoMapper;
    @Autowired
    StockBlockRtInfoMapper stockBlockRtInfoMapper;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    //必须保证该对象无状态
    private  HttpEntity<Object> httpEntity;
    @Override
    public void getInnerMarketInfo() {
        //采集原始数据
          //组装url地址,join将字符串集合以，拼接
        String url=stockInfoConfig.getMarketUrl() + String.join(",",stockInfoConfig.getInner());
          //维护请求头添加防盗链和用户表示
//        HttpHeaders headers = new HttpHeaders();
//          //防盗链
//        headers.add("Referer","https://finance.sina.com.cn/stock/");
//          //用户标识
//        headers.add("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
//          //维护请求实体对象
//        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
       //发起请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
        int statusCodeValue = responseEntity.getStatusCodeValue();
        if (statusCodeValue!=200) {
            //当前请求失败
            log.error("当前时间点：{}，采集数据失败。http状态码：{}",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),statusCodeValue);
            return;
        }
          //获取js数据
        String jsData = responseEntity.getBody();
        log.info("当前时间点：{}，采集原始数据：{}" ,DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),jsData);

        //正则解析数据
          //定义正则表达式
        String reg="var hq_str_(.+)=\"(.+)\";";
          //表达式编译
        Pattern pattern = Pattern.compile(reg);
          //匹配字符串
        Matcher matcher = pattern.matcher(jsData);
        List<StockMarketIndexInfo>entities=new ArrayList<>();
        while (matcher.find()){
            //获取大盘编码
            String marketCode = matcher.group(1);
            String otherInfo = matcher.group(2);
            //将other字符串以，切割，获取大盘详细信息
            String[] splitArr = otherInfo.split(",");
            //大盘名称
            String marketName=splitArr[0];
            //获取当前大盘的开盘点数
            BigDecimal openPoint=new BigDecimal(splitArr[1]);
            //前收盘点
            BigDecimal preClosePoint=new BigDecimal(splitArr[2]);
            //获取大盘的当前点数
            BigDecimal curPoint=new BigDecimal(splitArr[3]);
            //获取大盘最高点
            BigDecimal maxPoint=new BigDecimal(splitArr[4]);
            //获取大盘的最低点
            BigDecimal minPoint=new BigDecimal(splitArr[5]);
            //获取成交量
            Long tradeAmt=Long.valueOf(splitArr[8]);
            //获取成交金额
            BigDecimal tradeVol=new BigDecimal(splitArr[9]);
            //时间

            Date curTime = DateTimeUtil.getDateTimeWithoutSecond(splitArr[30] + " " + splitArr[31]).toDate();
            StockMarketIndexInfo entity = StockMarketIndexInfo.builder()
                    .id(idWorker.nextId())
                    .marketName(marketName)
                    .openPoint(openPoint)
                    .preClosePoint(preClosePoint)
                    .curPoint(curPoint)
                    .maxPoint(maxPoint)
                    .minPoint(minPoint)
                    .tradeAmount(tradeAmt)
                    .tradeVolume(tradeVol)
                    .marketCode(marketCode)
                    .curTime(curTime)
                    .build();
            entities.add(entity);
        }
        log.info("解析完毕");
        int count=stockMarketIndexInfoMapper.insertBatch(entities);
        if(count>0){
            //大盘数据采集完毕后通知backend工程刷新缓存
            //发送日期对象，接收通过接收的日期与当前日期作比较，能判断出数据延迟的时长，用于运维通知
            rabbitTemplate.convertAndSend("stockExchange","inner.market",new Date());
            log.info("当前执行时间：{}，插入大盘{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),entities);
        }else {
            log.error("当前执行时间：{}，插入大盘{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),entities);
        }

    }
    /*
     * @author songx
     * @description 采集个股的数据
     * @date 2024/5/27 15:43
     */
    @Override
    public void getStockRtInfo() {
        //获取所有个股的集合
        List<String> allCodes = stockBusinessMapper.getAllStockCodes();
        //添加大盘业务前缀 sh,sz
        allCodes=allCodes.stream().map(code->code.startsWith("6")?"sh"+code:"sz"+code).collect(Collectors.toList());
        System.out.println(allCodes);
        long startTime = System.currentTimeMillis();
        //将所有个股编码组成大的集合拆分成若干小的集合
        //将大集合切割成若干小集合分批次拉去数据
        Lists.partition(allCodes,15).forEach(codes->{
            //TODO:原始方案
//            //分批次采集
//            String url=stockInfoConfig.getMarketUrl()+String.join(",",codes);
////            //维护请求头添加防盗链和用户表示
////            HttpHeaders headers = new HttpHeaders();
////            //防盗链
////            headers.add("Referer","https://finance.sina.com.cn/stock/");
////            //用户标识
////            headers.add("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
////            //维护请求实体对象
////            HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
//            //发起请求
//            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
//            int statusCodeValue = responseEntity.getStatusCodeValue();
//            if (statusCodeValue!=200) {
//                //当前请求失败
//                log.error("当前时间点：{}，采集数据失败。http状态码：{}",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),statusCodeValue);
//                return;
//            }
//            //获取js数据
//            String jsData = responseEntity.getBody();
//            //调用工具类解析各个数据
//            List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
//            log.info("采集个股数据：{}",list);
//            //批量插入
//            int count=stockRtInfoMapper.insertBatch(list);
//            if(count>0){
//                log.info("当前执行时间：{}，插入个股有{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
//            }else {
//                log.error("当前执行时间：{}，插入个股{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
//            }
            //TODO:方案一，股票采集个股数据时将集合分片，然后分批次串行采集数据，效率不高，存在较高的采集延迟，引入多线程
            //
//            new Thread(()->{
//                //分批次采集
//                String url=stockInfoConfig.getMarketUrl()+String.join(",",codes);
//////            //维护请求头添加防盗链和用户表示
//////            HttpHeaders headers = new HttpHeaders();
//////            //防盗链
//////            headers.add("Referer","https://finance.sina.com.cn/stock/");
//////            //用户标识
//////            headers.add("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
//////            //维护请求实体对象
//////            HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
//            //发起请求
//            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
//            int statusCodeValue = responseEntity.getStatusCodeValue();
//            if (statusCodeValue!=200) {
//                //当前请求失败
//                log.error("当前时间点：{}，采集数据失败。http状态码：{}",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),statusCodeValue);
//                return;
//            }
//            //获取js数据
//            String jsData = responseEntity.getBody();
//            //调用工具类解析各个数据
//            List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
//            log.info("采集个股数据：{}",list);
//            //批量插入
//            int count=stockRtInfoMapper.insertBatch(list);
//            if(count>0){
//                log.info("当前执行时间：{}，插入个股有{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
//            }else {
//                log.error("当前执行时间：{}，插入个股{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
//            }
//            }).start();
            //todo:方案2引入线程池
            threadPoolTaskExecutor.execute(()->{
                String url=stockInfoConfig.getMarketUrl()+String.join(",",codes);
                ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
                int statusCodeValue = responseEntity.getStatusCodeValue();
                if (statusCodeValue!=200) {
                    //当前请求失败
                    log.error("当前时间点：{}，采集数据失败。http状态码：{}",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),statusCodeValue);
                    return;
                }
                //获取js数据
                String jsData = responseEntity.getBody();
                //调用工具类解析各个数据
                List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
                log.info("采集个股数据：{}",list);
                //批量插入
                int count=stockRtInfoMapper.insertBatch(list);
                if(count>0){
                    log.info("当前执行时间：{}，插入个股有{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
                }else {
                    log.error("当前执行时间：{}，插入个股{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
                }
            });
        });
        long takeTiem=System.currentTimeMillis()-startTime;
        log.info("本地采集花费时间：{}ms" ,takeTiem);

    }
    /*
     * @author songx
     * @description 采集板块数据
     * @date 2024/5/27 15:43
     */
    @Override
    public void getStockBlockInfo() {
        String result=restTemplate.getForObject(stockInfoConfig.getBlockUrl(),String.class);
        List<StockBlockRtInfo> stockBlockRtInfos = parserStockInfoUtil.parse4StockBlock(result);
        Lists.partition(stockBlockRtInfos,20).forEach(list->{
            //20个一组，批量插入
            stockBlockRtInfoMapper.insertBatch(list);
        });

    }

    /**
     * @author songx
     * @description bean生命周期初始化回调方法
     * @date 2024/5/29 20:52
     */
    @PostConstruct
    public void initBean(){
        //维护请求头添加防盗链和用户表示
        HttpHeaders headers = new HttpHeaders();
        //防盗链
        headers.add("Referer","https://finance.sina.com.cn/stock/");
        //用户标识
        headers.add("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
        //维护请求实体对象
         httpEntity = new HttpEntity<>(headers);

    }
}
