package com.hxx.stock.service;

/**
 * @Author song
 * @Date 2024/5/27 15:42
 * @Description: 定义股票采集数据服务接口
 */
public interface StockTimerTaskService {
    /*
     * @author songx
     * @description 获取国内大盘的实时数据信息
     * @date 2024/5/27 15:43
     */
    void getInnerMarketInfo();
    /*
     * @author songx
     * @description 采集个股的数据
     * @date 2024/5/27 15:43
     */
    void getStockRtInfo();
    /*
     * @author songx
     * @description 采集板块数据
     * @date 2024/5/27 15:43
     */
    void getStockBlockInfo();
}
