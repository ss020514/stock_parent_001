package com.hxx.stock.service.impl;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.hxx.stock.constant.StockConstant;
import com.hxx.stock.mapper.SysUserMapper;
import com.hxx.stock.pojo.entity.SysUser;
import com.hxx.stock.service.UserService;
import com.hxx.stock.util.IdWorker;
import com.hxx.stock.vo.req.LoginReqVo;
import com.hxx.stock.vo.resp.LoginResVo;
import com.hxx.stock.vo.resp.R;
import com.hxx.stock.vo.resp.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.mockito.internal.util.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author
 * @Date 2024/5/13 19:24
 * @Description:定义用户服务实现
 */
@Service("userService")
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private RedisTemplate redisTemplate;

    /*
     * @author songx
     * @description TODO: 根据用户名查询用户信息
     * @date 2024/5/13 19:27
     */
    @Override
    public SysUser findByUserName(String userName) {
        return sysUserMapper.findUserInfoByUserName(userName);
    }

    @Override
    public R<LoginResVo> login(LoginReqVo vo) {
//1.判断输入的参数是否合法
        if(vo==null|| StringUtils.isBlank(vo.getUsername())|| StringUtils.isBlank(vo.getPassword())){
            return R.error(ResponseCode.DATA_ERROR);
        }
        //判断验证码是否存在
        if(StringUtils.isBlank(vo.getCode())||StringUtils.isBlank(vo.getSessionId())){
            return R.error(ResponseCode.CHECK_CODE_ERROR);
        }
        //判断redis保存的验证码和输入的验证码是否相同(比较值忽略大小写)
        String redisCode =(String) redisTemplate.opsForValue().get(StockConstant.CHECK_PREFIX+vo.getSessionId());
        if (StringUtils.isBlank(redisCode)) {
            //验证码过期
            return R.error(ResponseCode.CHECK_COOE__TIMEOUT);
        }
        //验证码错误
        if (! redisCode.equalsIgnoreCase(vo.getCode())) {
            return R.error(ResponseCode.CHECK_CODE_ERROR);
        }

//2.跟据用户名去数据库查询用户信息，获取密码的名文
        SysUser user=sysUserMapper.findByUserName(vo.getUsername());
//3调用密码匹配器匹配输入的密码和名文密码是否匹配
        if(user==null||passwordEncoder.matches(vo.getPassword(),user.getPassword())){
            return R.error(ResponseCode.USERNAME_OR_PASSWORD_ERROR);
        }
//4.响应数据
        LoginResVo loginResVo = new LoginResVo();
        BeanUtils.copyProperties(user,loginResVo);
        return R.ok(loginResVo);
    }
    /*
     * @author songx
     * @description TODO：生成图片验证码功能
     * @date 2024/5/15 16:29
     */
    @Override
    public R<Map> getCaptchaCode() {
        //1.生成图片验证码并且分别设置图片的宽度，高度，验证码长度，干扰线
        LineCaptcha captcha = CaptchaUtil.createLineCaptcha(250, 40, 4, 5);
        captcha.setBackground(Color.LIGHT_GRAY);
        //获取校验码
        String checkCode = captcha.getCode();
        //获取经过base64编码处理的图片数据
        String imageDta = captcha.getImageBase64();
        //生成sessionId 转换String防止精度过长精度丢失
        String sessionId = String.valueOf(idWorker.nextId());
        //将session做为key，校验码做为value保存到redis中
        //使用redis模仿session行为，并设置过期时间 CK方便后期维护
        log.info("当前生成图片校验码：{}，会话id:{}",checkCode,sessionId);
        redisTemplate.opsForValue().set(StockConstant.CHECK_PREFIX +sessionId,checkCode,5, TimeUnit.MINUTES);
        //组装数据
        Map <String,String>data=new HashMap();
        data.put("imageData",imageDta);
        data.put("sessionId",sessionId);
        //响应数据
        return R.ok(data);
    }
}