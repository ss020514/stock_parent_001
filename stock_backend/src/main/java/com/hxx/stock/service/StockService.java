package com.hxx.stock.service;

import com.hxx.stock.pojo.domain.*;
import com.hxx.stock.vo.resp.PageResult;
import com.hxx.stock.vo.resp.R;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @Author
 * @Date 2024/5/19 13:59
 * @Description:股票服务接口
 */
public interface StockService {
    /*
     * @author songx
     * @description TODO：获取国内大盘最新数据
     * @date 2024/5/19 13:57
     */
    R<List<InnerMarketDomain>> getInnerMarketInfo();

    R<List<StockBlockDomain>> sectorAll();

    R<PageResult<StockUpdownDomain>> getStockInfoByPage(Integer page, Integer pageSize);

    R<Map<String, List>> getStockUpDownCount();

    void exportStockUpDown(Integer page, Integer pageSize, HttpServletResponse response);

    R<List<StockUpdownDomain>> stockIncrease();

    R<Map<String, List>> getComparedStockTradeAmt();

    R<Map> getIncreaseRangeInfo();

    R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode);

    R<List<Stock4EvrDayDomain>> getStockScreenDKLine(String stockCode);
}
