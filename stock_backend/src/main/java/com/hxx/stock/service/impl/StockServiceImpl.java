package com.hxx.stock.service.impl;

import cn.hutool.core.lang.Snowflake;
import com.alibaba.excel.EasyExcel;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hxx.stock.mapper.StockBlockRtInfoMapper;
import com.hxx.stock.mapper.StockMarketIndexInfoMapper;
import com.hxx.stock.mapper.StockRtInfoMapper;
import com.hxx.stock.pojo.domain.*;
import com.hxx.stock.pojo.vo.StockInfoConfig;
import com.hxx.stock.service.StockService;
import com.hxx.stock.util.DateTimeUtil;
import com.hxx.stock.vo.resp.PageResult;
import com.hxx.stock.vo.resp.R;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;

/**
 * @Author
 * @Date 2024/5/19 14:01
 * @Description:股票服务实现
 */
@ApiModel(description = "股票服务实现")
@Service
@Slf4j
public class StockServiceImpl implements StockService {
    @ApiModelProperty(hidden = true)
    @Autowired
    StockInfoConfig stockInfoConfig;
    @ApiModelProperty(hidden = true)
    @Autowired
    StockMarketIndexInfoMapper stockMarketIndexInfoMapper;
    @ApiModelProperty(hidden = true)
    @Autowired
    StockBlockRtInfoMapper stockBlockRtInfoMapper;
    @ApiModelProperty(hidden = true)
    @Autowired
    StockRtInfoMapper stockRtInfoMapper;
    /*
     * @author songx
     * @description TODO：注入本地缓存bena
     * @date 2024/5/19 13:57
     */
    @Autowired
    private Cache<String ,Object>caffeineCache;
    /*
     * @author songx
     * @description TODO：获取国内大盘最新数据
     * @date 2024/5/19 13:57
     */
    @Override
    public R<List<InnerMarketDomain>> getInnerMarketInfo() {
        //默认从本地缓存中拿去数据，如果不存在则去数据库里拿
        //在开盘周期内本地缓存默认1分分钟
        R<List<InnerMarketDomain>> result= (R<List<InnerMarketDomain>>) caffeineCache.get("innerMarketKey", key->{
            Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
            curDate =DateTime.parse("2021-12-28 09:31:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
            //mock data 等后续完成Job工程，再将代码删除

            //获取大盘编码集合
            List<String> mCodes = stockInfoConfig.getInner();
            //调用mapper查询数据
            List<InnerMarketDomain> data=stockMarketIndexInfoMapper.getMarketInfo(curDate,mCodes);
            //封装并相应
            return R.ok(data);
        });
        //获取股票最新交易时间点（精确到分钟，秒和毫秒置为0）
        return result;
    }

    @Override
    public R<List<StockBlockDomain>> sectorAll() {

        Date curDate=DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        curDate=DateTime.parse("2022-01-14 16:57:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        List<StockBlockDomain> data=stockBlockRtInfoMapper.sectorAll(curDate);
        return R.ok(data);
    }
    /*
     * @author songx
     * @description TODO：分页查询最新的股票数据
     * @date 2024/5/20 17:09
     */
    @Override
    public R<PageResult<StockUpdownDomain>> getStockInfoByPage(Integer page, Integer pageSize) {
        //获取最新股票交易时间
        Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        curDate=DateTime.parse("2022-07-07 14:55:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //设置分页参数
        PageHelper.startPage(page,pageSize);
        List<StockUpdownDomain> pageData=stockRtInfoMapper.getStockInfoByTime(curDate);
        //组装pageResult对象
        PageInfo<StockUpdownDomain> pageInfo = new PageInfo<>(pageData);
        PageResult<StockUpdownDomain> pageResult = new PageResult<>(pageInfo);
        return R.ok(pageResult);
    }
    /*
     * @author songx
     * @description 统计最新股票交易日内每分钟的涨跌停的股票数据
     * @date 2024/5/20 21:49
     */
    @Override
    public R<Map<String, List>> getStockUpDownCount() {
        //获取最新股票的交易时间点（截止时间）
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        curDateTime=DateTime.parse("2022-01-06 14:55:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date endDate=curDateTime.toDate();
        //获取最新交易时间点对应的开盘时间点
        Date starDate = DateTimeUtil.getOpenDate(curDateTime).toDate();
        //统计涨停数据
        List<Map> upList=stockRtInfoMapper.getStockUpdownCount(starDate,endDate,1);
        List<Map> downList=stockRtInfoMapper.getStockUpdownCount(starDate,endDate,0);
        //组装数据
        HashMap<String,List> info=new HashMap<>();
        info.put("upList",upList);
        info.put("downList",downList);
        //响应数据
        return  R.ok(info);
    }

    @Override
    public void exportStockUpDown(Integer page, Integer pageSize, HttpServletResponse response) {
        //获取分页数据
        R<PageResult<StockUpdownDomain>> r = this.getStockInfoByPage(page, pageSize);
        List<StockUpdownDomain> rows = r.getData().getRows();
        //将数据导出excel
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        try {
            String fileName = URLEncoder.encode("股票信息表", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream(), StockUpdownDomain.class).sheet("股票涨幅信息").doWrite(rows);
        } catch (IOException e) {
           log.error("当前页码{}，每页大小{}，当前时间，异常信息{}",page,pageSize,DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),e.getMessage());

    }
    }

    @Override
    public R<List<StockUpdownDomain>> stockIncrease() {
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        curDateTime=DateTime.parse("2022-01-06 14:55:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date curDate=curDateTime.toDate();
        List<StockUpdownDomain> data=stockRtInfoMapper.stockIncreaseBy4(curDate);
        return R.ok(data);
    }

    @Override
    public R<Map<String, List>> getComparedStockTradeAmt() {
        //获取最新股票交易日的日期范围
        DateTime tEndTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        tEndTime=DateTime.parse("2021-12-27 15:00:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date tEndDate = tEndTime.toDate();
        //开盘时间
        Date tStartDate = DateTimeUtil.getOpenDate(tEndTime).toDate();
        //获取T-1的时间范围
        DateTime preEndDateTime = DateTimeUtil.getPreviousTradingDay(tEndTime);
        preEndDateTime=DateTime.parse("2021-12-26 15:00:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date preEndDate = preEndDateTime.toDate();
        Date preStartDate = DateTimeUtil.getOpenDate(preEndDateTime).toDate();
        //调用Mapper查询
        //t日
        List<Map> tData=stockMarketIndexInfoMapper.getSumAmtInfo(tStartDate,tEndDate,stockInfoConfig.getInner());
        //T-1
        List<Map> preTData=stockMarketIndexInfoMapper.getSumAmtInfo(preStartDate,preEndDate,stockInfoConfig.getInner());
        //组装数据
        HashMap<String,List>info=new HashMap<>();
        info.put("amtList",tData);
        info.put("yesAmtList",preTData);
        return R.ok(info);
    }

    @Override
    public R<Map> getIncreaseRangeInfo() {
        //获取当前最新的股票交易时间点
       DateTime curDateTime= DateTimeUtil.getLastDate4Stock(DateTime.now());
        curDateTime=DateTime.parse("2022-01-06 09:55:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date curDate=  curDateTime.toDate();
        //调用mapper
        List<Map> infos=stockRtInfoMapper.getIncreaseRangeInfoByDate(curDate);
        //获取有序标题集合
        List<String> upDownRange = stockInfoConfig.getUpDownRange();
        //将顺序的涨幅区间内的每个元素转换成map对象
        List<Map> allInfo=new ArrayList<>();
        for (String title : upDownRange) {
            Map tmp=null;
            for (Map info : infos) {
                if (info.containsValue(title)) {
                    tmp = info;
                    break;
                }
            }
               if(tmp==null){
                   tmp=new HashMap();
                   tmp.put("count",0);
                   tmp.put("title",title);
               }
                allInfo.add(tmp);
            }

        //组装数据
        HashMap<String,Object> data=new HashMap<>();
        data.put("time",curDateTime.toString("yyyy-MM-dd HH:mm:ss"));
        data.put("infos",allInfo);
        return R.ok(data);
    }
    /*
     * @author songx
     * @description 获取指定股票T日分时数据
     * @param stockCode 个股股票编码
     * @date 2024/5/22 19:58
     */
    @Override
    public R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode) {
        //获取T日最新交易时间点
        DateTime endDataTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        endDataTime=DateTime.parse("2021-12-30 14:30:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date endDate = endDataTime.toDate();
        DateTime startDateTime = DateTimeUtil.getOpenDate(endDataTime);
        Date startDate = startDateTime.toDate();
        //查询
        List<Stock4MinuteDomain> data=stockRtInfoMapper.getStock4MinuteInfo(startDate,endDate,stockCode);
        return R.ok(data);

    }
    /*
     * @author songx
     * @description 统计查询指定股票日K线
     * @param stockCode 个股股票编码
     * @date 2024/5/22 19:58
     */
    @Override
    public R<List<Stock4EvrDayDomain>> getStockScreenDKLine(String stockCode) {
        //获取统计日K线的时间范围
        //获取截止时间
        DateTime endDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        endDateTime=DateTime.parse("2022-6-6 14:30:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date endDate=endDateTime.toDate();
        //起始时间,往前推三个月
        DateTime startDateTime = endDateTime.minusMonths(3);
        startDateTime=DateTime.parse("2022-1-1 9:30:00" ,DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date startDate = startDateTime.toDate();
        //调用mapper获取指定数据
        //List<Stock4EvrDayDomain> data=stockRtInfoMapper.getStock4DKLine(startDate,endDate,stockCode);
        //方案2：先获取指定日期范围内的收盘时间点集合
        List<Date> closeDates = stockRtInfoMapper.getCloseDates(startDate,endDate,stockCode);
        //根据收盘时间获取日K数据
        List<Stock4EvrDayDomain> data = stockRtInfoMapper.getStockCreenDkLineData(stockCode, closeDates);
        //返回数据
        return R.ok(data);
    }
}
