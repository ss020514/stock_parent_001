package com.hxx.stock.service;

import com.hxx.stock.pojo.entity.SysUser;
import com.hxx.stock.vo.req.LoginReqVo;
import com.hxx.stock.vo.resp.LoginResVo;
import com.hxx.stock.vo.resp.R;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @Author song
 * @Date 2024/5/13 19:22
 * @Description:
 */
public interface UserService {
    /*
     * @author songx
     * @description TODO：根据用户名称查询用户信息
     * @date 2024/5/13 19:23
     */
    SysUser findByUserName(String userName);
/**
 * @author songx
 * @description TODO：用户登入功能
 * @date 2024/5/14 14:06
 */
public R<LoginResVo> login(LoginReqVo vo);
/*

 * @author songx
 * @description TODO：生成图片验证码功能
 * @date 2024/5/15 16:29
 */
    public R<Map>getCaptchaCode();
}
