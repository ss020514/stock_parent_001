package com.hxx.stock.mq;

import com.github.benmanes.caffeine.cache.Cache;
import com.hxx.stock.service.StockService;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author song
 * @Date 2024/5/30 17:19
 * @Description:定义股票相关mq监听
 */
@Component
@Slf4j
public class StockMsgListener {
    @Autowired
    Cache<String,Object> caffeineCache;
    @Autowired
    StockService stockService;
    @RabbitListener(queues = "innerMarketQueue")
    public void refreshInnerMarketInfo(Date starTime){
        //统计当前时间点和发送时间点的插值，如果超过一分钟，则告警
        //获取时间毫秒差值
        long diffTime= DateTime.now().getMillis()-new DateTime(starTime).getMillis();
        if (diffTime>60000) {
            log.error("大盘发送消息时间：{},延迟时间：{}ms",new DateTime(starTime).toString("yyyy-MM-dd HH:mm:ss"),diffTime);
        }
        //刷新缓存
        //剔除旧的数据
        caffeineCache.invalidate("innerMarketKey");
        //调用服务方法刷新数据
        stockService.getInnerMarketInfo();
    }
}
