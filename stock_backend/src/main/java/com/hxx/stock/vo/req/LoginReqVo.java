package com.hxx.stock.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/**
 * @Author song
 * @Date 2024/5/14 13:09
 * @Description:登入请求vo
 */
@ApiModel()
@Data
public class LoginReqVo {
//    用户名
   @ApiModelProperty(value = "用户名")
    private  String username;
    //密码
    @ApiModelProperty(value = "密码")
    private  String password;
    //验证码
    @ApiModelProperty(value = "验证码")
    private  String code;
    //会话id
    @ApiModelProperty(value = "会话id")
    private  String sessionId;
}
