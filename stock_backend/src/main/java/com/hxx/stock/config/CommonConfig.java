package com.hxx.stock.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.hxx.stock.pojo.vo.StockInfoConfig;
import com.hxx.stock.util.IdWorker;
import com.hxx.stock.util.ParserStockInfoUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.cbor.MappingJackson2CborHttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @Author song
 * @Date 2024/5/14 13:50
 * @Description:定义公共配置bean
 */
@Configuration
@EnableConfigurationProperties({StockInfoConfig.class})//开启对象相关配置类的开启
public class CommonConfig {
    /*
     * @author songx
     * @description TODO：定义密码加密匹配器
     * @date 2024/5/14 13:53
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    /*
     * @author songx
     * @description TODO：基于雪花算法的工具类保证生成的id唯一
     * @date 2024/5/15 15:18
     */
    @Bean
    public IdWorker idWorker(){
        //基于运维人员对机房和机器的编号规划自行约定
        return new IdWorker(1l,2l);
    }
    @Bean
    public ParserStockInfoUtil parserStockInfoUtil(IdWorker idWorker){
        return  new ParserStockInfoUtil(idWorker);
    }
    /*
     * @author songx
     * @description TODO：统一定义long序列化转Sting设置（所有的long序列化成String）
     * @date 2024/5/15 15:18
     */
//    @Bean
//    public MappingJackson2CborHttpMessageConverter mappingJackson2CborHttpMessageConverter(){
//        //构建Http信息转换对象
//        MappingJackson2CborHttpMessageConverter converter = new MappingJackson2CborHttpMessageConverter();
//        ObjectMapper objectMapper = new ObjectMapper();
//        //反系列化忽略位置属性
//        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
//        SimpleModule simpleModule = new SimpleModule();
//        //long序列化String
//        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
//        simpleModule.addSerializer(Long.TYPE,ToStringSerializer.instance);
//        //注册转化器
//        objectMapper.registerModule(simpleModule);
//        //设置序列化实现
//        converter.setObjectMapper(objectMapper);
//        return converter;
//    }
}
