package com.hxx.stock.controller;

import com.hxx.stock.pojo.domain.*;
import com.hxx.stock.service.StockService;
import com.hxx.stock.vo.resp.PageResult;
import com.hxx.stock.vo.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @Author song
 * @Date 2024/5/19 13:42
 * @Description:定义股票相关接口控制器
 *
 */
@Api(value = "/api/quot", tags = {"定义股票相关接口控制器"})
@RestController
@RequestMapping("/api/quot")
public class StockController {
    @Autowired
    private StockService stockService;
    /*
     * @author songx
     * @description TODO：获取国内大盘最新数据
     * @date 2024/5/19 13:57
     */
    @ApiOperation(value = "获取国内大盘最新数据", notes = "获取国内大盘最新数据", httpMethod = "GET")
    @GetMapping("/index/all")
    public R<List<InnerMarketDomain>> getInnerMarketInfo(){
        return stockService.getInnerMarketInfo();
    }
    /*
     * @author songx
     * @description TODO：获取沪深两市板块最新数据，以交总金额降序穿，取前十条数据
     * @date 2024/5/19 13:57
     */
    @ApiOperation(value = "获取沪深两市板块最新数据，以交总金额降序穿，取前十条数据", notes = "获取沪深两市板块最新数据，以交总金额降序穿，取前十条数据", httpMethod = "GET")
    @GetMapping("/sector/all")
    public R<List<StockBlockDomain>> sectorAll(){
      return stockService.sectorAll();
    }
    /*
     * @author songx
     * @description TODO：分页查询最新的股票数据
     * @date 2024/5/20 17:09
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "page", value = ""),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "")
    })
    @ApiOperation(value = "分页查询最新的股票数据", notes = "分页查询最新的股票数据", httpMethod = "GET")
    @GetMapping("/stock/all")
    public R<PageResult<StockUpdownDomain>> getStockInfoByPage(@RequestParam(value = "page",required = false,defaultValue = "1") Integer page,
                                                               @RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize){
    return stockService. getStockInfoByPage(page,pageSize);
    }
    /*
     * @author songx
     * @description 统计最新股票交易日内每分钟的涨跌停的股票数据
     * @date 2024/5/20 21:49
     */
    @ApiOperation(value = "统计最新股票交易日内每分钟的涨跌停的股票数据", notes = "统计最新股票交易日内每分钟的涨跌停的股票数据", httpMethod = "GET")
    @GetMapping("/stock/updown/count")
    public R<Map<String,List>> getStockUpDownCount(){
        return stockService.getStockUpDownCount();
    }
    /*
     * @author songx
     * @description 导出指定页码的最新股票信息
     * @param page:当前页
     * @param pageSize:每页大小
     * @param response
     * @date 2024/5/22 10:24
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "page", value = ""),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "")
    })
    @ApiOperation(value = "导出指定页码的最新股票信息", notes = "导出指定页码的最新股票信息", httpMethod = "GET")
    @GetMapping("/stock/export")
    public void exportStockUpDown(@RequestParam(value = "page",required = false,defaultValue = "1") Integer page,
                                  @RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize, HttpServletResponse response){
         stockService.exportStockUpDown(page,pageSize,response);
    }
    /*

     * @author songx
     * @description 股票数据涨幅排行榜
     * @date 2024/5/22 19:53
     */
    @ApiOperation(value = "股票数据涨幅排行榜", notes = "股票数据涨幅排行榜", httpMethod = "GET")
    @GetMapping("/stock/increase")
    public R<List<StockUpdownDomain>> stockIncrease(){
        return stockService.stockIncrease();
    }
    /*
     * @author songx
     * @description 统计大盘在T日和T-1日每分钟交易量的统计
     * @date 2024/5/22 19:58
     */
    @ApiOperation(value = "统计大盘在T日和T-1日每分钟交易量的统计", notes = "统计大盘在T日和T-1日每分钟交易量的统计", httpMethod = "GET")
    @GetMapping("/stock/tradeAmt")
    public R<Map<String,List>> getComparedStockTradeAmt(){
        return stockService.getComparedStockTradeAmt();
    }
    /*
     * @author songx
     * @description 统计最新交易时间点下股票在各个涨幅区间的数量
     * @date 2024/5/22 19:58
     */
    @ApiOperation(value = "统计最新交易时间点下股票在各个涨幅区间的数量", notes = "统计最新交易时间点下股票在各个涨幅区间的数量", httpMethod = "GET")
    @GetMapping("/stock/updown")
    public R<Map> getIncreaseRangeInfo(){
        return  stockService.getIncreaseRangeInfo();
    }
    /*
     * @author songx
     * @description 获取指定股票T日分时数据
     * @param stockCode 个股股票编码
     * @date 2024/5/22 19:58
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "code", value = "个股股票编码", required = true)
    })
    @ApiOperation(value = "获取指定股票T日分时数据", notes = "获取指定股票T日分时数据", httpMethod = "GET")
    @GetMapping("/stock/screen/time-sharing")
    public R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(@RequestParam(value = "code",required = true) String stockCode){
        return stockService.getStockScreenTimeSharing(stockCode);
    }
    /*
     * @author songx
     * @description 统计查询指定股票日K线
     * @param stockCode 个股股票编码
     * @date 2024/5/22 19:58
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "code", value = "个股股票编码", required = true)
    })
    @ApiOperation(value = "统计查询指定股票日K线", notes = "统计查询指定股票日K线", httpMethod = "GET")
    @GetMapping("/stock/screen/dkline")
    public R<List<Stock4EvrDayDomain>> getStockScreenDKLine(@RequestParam(value = "code" ,required = true) String stockCode){
     return stockService.getStockScreenDKLine(stockCode);
    }
}
