package com.hxx.stock.controller;

import com.hxx.stock.pojo.entity.SysUser;
import com.hxx.stock.service.UserService;
import com.hxx.stock.vo.req.LoginReqVo;
import com.hxx.stock.vo.resp.LoginResVo;
import com.hxx.stock.vo.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author song
 * @Date 2024/5/13 19:28
 * @Description:应用用户web层接口资源bean
 */
@RestController
@RequestMapping("/api")
@Api(tags = "用户相关接口处理")
public class UserController {
    @Autowired
    private UserService userService;
    /**
     * @author songx
     * @description TODO：跟据用户名称查询
     * @date 2024/5/13 19:33
     */
    @ApiOperation(value = "跟据用户名查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName",value = "用户名",dataType = "String",required = true,type = "path")
    })
    @GetMapping("/user/{userName}")
    public SysUser getUserByUserName(@PathVariable("userName") String userName){
        return userService.findByUserName(userName);
    }
    /**
     * @author songx
     * @description TODO：用户登入功能
     * @date 2024/5/14 14:06
     */
    @ApiOperation(value = "用户登入")
    @PostMapping("/login")
    public R<LoginResVo> login(@RequestBody LoginReqVo vo){
        return userService.login(vo);
    }
    @ApiOperation(value = "验证码")
    @GetMapping("/captcha")
    public R<Map>getCaptchaCode(){
        return userService.getCaptchaCode();
    }
}
