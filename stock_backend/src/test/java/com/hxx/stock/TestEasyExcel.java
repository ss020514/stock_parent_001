package com.hxx.stock;

import com.alibaba.excel.EasyExcel;
import com.hxx.stock.pojp.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author song
 * @Date 2024/5/21 21:24
 * @Description:easyExcel测试类
 */
public class TestEasyExcel {
    public List<User> init(){
        //组装数据
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setAddress("上海"+i);
            user.setUserName("张三"+i);
            user.setBirthday(new Date());
            user.setAge(10+i);
            users.add(user);
        }
        return users;
    }
    @Test
    public void test01(){
        List<User> users = this.init();
        EasyExcel.write("C:\\Users\\songx\\Desktop\\data\\test.xls",User.class)
                .sheet("用户信息").doWrite(users);
    }

}
