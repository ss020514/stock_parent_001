package com.hxx.stock;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hxx.stock.mapper.SysUserMapper;
import com.hxx.stock.pojo.entity.SysUser;
import com.hxx.stock.pojo.vo.StockInfoConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @Author song
 * @Date 2024/5/20 15:39
 * @Description:分页测试类
 */
@SpringBootTest
public class TestPageHelper {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private StockInfoConfig stockInfoConfig;
    /*
     * @author songx
     * @description TODO：测试分页
     * @date 2024/5/20 15:43
     */
    @Test
    public void test01(){
        Integer page=2;
        Integer pagSize=5;
        PageHelper.startPage(page,pagSize);
        List<SysUser> all = sysUserMapper.findAll();
        //将查询的对象封装到pageInfo就可以获取分页的各种数据
        PageInfo<SysUser> pageInfo = new PageInfo<>();
        int pageNum = pageInfo.getPageNum();//获取当前页
        int pages = pageInfo.getPages();//获取总页数
        List<SysUser> list = pageInfo.getList();//获取当前页的具体内容
        System.out.println(all);
    }
    @Test
    public void test02(){

    }
}
