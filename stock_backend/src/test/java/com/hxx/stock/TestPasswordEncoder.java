package com.hxx.stock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @Author song
 * @Date 2024/5/14 13:54
 * @Description:
 */
@SpringBootTest
public class TestPasswordEncoder {
    @Autowired
    PasswordEncoder passwordEncoder;
    /**
     * @author songx
     * @description TODO：测试密码加密
     * @date 2024/5/14 13:55
     */
    @Test
    public void testPwd(){
        String pwd="123";
        String encode=passwordEncoder.encode(pwd);
        //$2a$10$KsDzJ2qD2aklNu3o4AmRGOOykhaEDHBj35KBQIiS/0yudysGv0Qd2
        System.out.println(encode);
        Boolean flag=passwordEncoder.matches(pwd,encode);
        System.out.println(flag?"匹配成功":"匹配失败");


    }
}
